const express = require('express');
const app = express();

app.get('/', (req, res) => {
  debugger // if you attach the debugger the code will be stopped here
  res.send('Hello World from Nodejs Express API!');
});

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});