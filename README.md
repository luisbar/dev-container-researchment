This is a monorepo which contains projects with different languages and they are bootstrapped by using [Dev Container](https://code.visualstudio.com/docs/devcontainers/containers)

## How to run it?
- First you have to have Docker installed and running
- Second you have to have installed the [Dev Container extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
- Then open the command bar by using `command + shift + p`
- Look for the following command [Dev Containers: Add Dev Container Configuration Files...](https://code.visualstudio.com/remote/advancedcontainers/configure-separate-containers)
- Select the folder
  - nodejs-express-api: it runs on port 3000
  - python-flask-api: it runs on port 5000

## How was configured this monorepo?
- First you have to have Docker installed and running
- Second you have to have installed the [Dev Container extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
- Then open the command bar by using `command + shift + p`
- Look for the following command [Dev Containers: Add Dev Container Configuration Files...](https://code.visualstudio.com/remote/advancedcontainers/configure-separate-containers)
- Select the folder
- Finally follow the wizard
  - [Here](https://containers.dev/templates) you can find the list of templates/images
  - [Here](https://containers.dev/features) you can find the list of features

## How to attach to Visual Studio Code debugger?
- Go to debug section and choose `Attach to nodejs-express-api` option in the dropdown at the top left side
- Then open `http://localhost:3000` and you will see the debugger in action
![Debugger](/screenshots/debugger.png)

## Hints and tips
- You can use templates for running a container (python-flask-api) or you can use your own Dockerfile (nodejs-express-api)
- If you want to ensure all devs have installed certain extensions or Visual Studio Code configured with a specific configuration, you can use the [customizations](https://containers.dev/supporting#editors) property
  ```json
	"customizations": {
		// Configure properties specific to VS Code.
		"vscode": {
			// Set *default* container specific settings.json values on container create.
			"settings": {
				"editor.tabSize": 2,
			},
			"extensions": [
				"eamodio.gitlens",
			],
		}
	}
  ```
- Here you have an image in order to get the extension id
  ![Extension id](/screenshots/extension-id.png)
- Dev Containers has [features](https://containers.dev/features) to be enabled out ot the box, for instance if you want to have installed ngrok on your container you have to add it to `features` property as you can see down below:
  ```json
	"features": {
		"ghcr.io/jckimble/devcontainer-features/ngrok:3.1.1": {}
	},
	```
- In order to attach the VS Code debugger to a running container you have to configure the debugger executing the command displayed in the image, and it will create a `launch.json` file
  ![Extension id](/screenshots/debugger-config.png)
  